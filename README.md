# Information Engineering Service Owner demo repo

This repository is a Service Owner repository created for the Information Engineering team.

The url of the created netorca instance is https://api-university.demo.netorca.io/v1/

The username and password you can use to log in as this team is:
- username: lb_team1
- password: aut0m8t1on

University of Padova
Department of Computer Engineering
Faculty and student office

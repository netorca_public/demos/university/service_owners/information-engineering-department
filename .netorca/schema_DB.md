# Database Service

## Introduction

This service is designed to provide students, researchers, and faculty with secure, reliable, and flexible database solutions for their projects. Users can choose between different types of databases to suit their specific needs.

## Features

- **Database Options**: Choose from a variety of database types, including:
  - **Relational Databases**:
    - MySQL
    - PostgreSQL
    - Oracle
    - Microsoft SQL Server
  - **NoSQL Databases**:
    - MongoDB
    - Cassandra
    - Redis
    - CouchDB

- **Secure Access**: Secure login and data storage to protect your data.
- **Consistency and Reliability**: Ensures data consistency and reliability across all database types.
- **Data Protection**: Ensure your data remains confidential with encrypted connections and secure data storage.
- **Regular Backups**: Automated backups to prevent data loss and facilitate disaster recovery.
- **Cost Efficiency**: Utilize university resources without the need for personal investment in database infrastructure.


## How to access

- After submission and process via NetOrca, the ip and credentials to the database will be available at Deployed Items in the service item detail page

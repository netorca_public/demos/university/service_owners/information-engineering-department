# Virtual Machine Service for Neuroscience Students

## Introduction

This service is designed to provide students with easy access to virtual machines preinstalled with all the necessary software and tools for their coursework and research. This document will guide you through the features, benefits, and usage of the service.

## Features

- **Preinstalled Software**: Each VM comes with essential neuroscience software, including:
  - MATLAB
  - Python with scientific libraries (NumPy, SciPy, Pandas)
  - Brain imaging tools (FSL, SPM, FreeSurfer)
  - Data analysis tools (R, Jupyter Notebook)
  - Simulation tools (NEURON, Brian2)

- **User-Friendly Interface**: Easy-to-use web portal for creating, managing, and accessing your VMs.

- **Custom Configurations**: Options to request additional software or specific configurations tailored to your needs.

- **Secure Access**: Secure login and data storage to ensure your research and coursework remain confidential.

- **24/7 Availability**: Access your VM anytime, anywhere within the university network.

## Benefits

- **Consistency**: Ensures all students have access to the same software environment, reducing compatibility issues.

- **Convenience**: No need to install and configure software on personal devices.

- **Resource Optimization**: Efficient use of university computing resources, allowing for high-performance computing needs.

- **Focus on Learning**: Spend more time on learning and research instead of software setup and troubleshooting.

- **Software Usage**: All preinstalled software will be ready to use. You can save your work and data within the VM environment.


## How to access

- **VM Credentials**: After submission and process via NetOrca, the credentials to the machine will be available at Deployed Items in the service item detail page

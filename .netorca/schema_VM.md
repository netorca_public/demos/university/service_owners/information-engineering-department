# Virtual Machine Service for Network and Web Applications

## Introduction

A simple virtual machine created on the university cloud. This service is designed to provide students, researchers, and faculty with easy access to simple VMs that are ideal for developing and testing network and web applications.

## Features

- **Connect to the existing storage**: If you already have an II_SERVICE storage, you can connect it to this VM via references
- **Average Delivery Time**: 30 minutes
- **Custom Configurations**: Options to request additional software or specific configurations tailored to your needs.
- Usage is subject to the terms and conditions

## How to access

- **VM Credentials**: After submission and process via NetOrca, the credentials to the machine will be available at Deployed Items in the service item detail page

# Virtual Machine Service for AI Model Training with NVIDIA GPUs

## Introduction

This service is tailored for students, researchers, and faculty who require high-performance NVIDIA GPUs for training artificial intelligence (AI) and machine learning (ML) models. This document will guide you through the features, benefits, and usage of the service.

## Features

- **High-Performance GPUs**: Choose from a variety of NVIDIA GPUs to match your computational needs, including:
  - NVIDIA Tesla V100
  - NVIDIA A100
  - NVIDIA RTX 3080
  - NVIDIA RTX 3090

- **Preinstalled AI Frameworks**: Each VM comes with popular AI and ML frameworks preinstalled, such as:
  - TensorFlow
  - PyTorch
  - Keras
  - MXNet
  - scikit-learn

- **Optimized Environments**: Configurations are optimized for deep learning workloads to maximize performance.

- **Scalable Resources**: Easily scale your VM resources as your project requirements grow.

- **Secure Access**: Secure login and data storage to ensure your research and models remain confidential.

- **24/7 Availability**: Access your VM anytime, anywhere within the university network.

## How to access

- **VM Credentials**: After submission and process via NetOrca, the credentials to the machine will be available at Deployed Items in the service item detail page
